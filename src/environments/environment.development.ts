export const environment = {};

export const SpotifyConfiguration = {
  // https://developer.spotify.com/documentation/general/guides/authorization/implicit-grant/
  apiSpotify:"https://api.spotify.com/v1",
  clientId: 'f5b4f6eee9dc4328b0fce3d24f914ff3',
  authEndpoint: 'https://accounts.spotify.com/authorize',
  redirectUrl: 'https://spotify-webapp-clone.web.app/login/',
  scopes: [
    // https://developer.spotify.com/documentation/general/guides/authorization/scopes/
    "user-read-currently-playing", // musica tocando agora.
    "user-read-recently-played", // ler musicas tocadas recentemente
    "user-read-playback-state", // ler estado do player do usuario
    "user-top-read", // top artistas e musicas do usuario
    "user-modify-playback-state", // alterar do player do usuario.
    "user-library-read", // ler biblioteca dos usuarios
    "playlist-read-private", // ler playlists privads
    "playlist-read-collaborative" // ler playlists colaborativas
  ]
}
