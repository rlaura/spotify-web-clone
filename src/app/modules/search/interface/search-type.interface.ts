export interface searchState {
  searchParam : string;
  searchRoute : string;
  searchName  : string;
}