import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchRoutingModule } from './search-routing.module';
import { SearchPageComponent } from './pages/search-page/search-page.component';
import { SearchComponent } from './components/search/search.component';
import { CardCircleComponent } from 'src/app/shared/components/card-circle/card-circle.component';
import { CardSquareComponent } from 'src/app/shared/components/card-square/card-square.component';
import { ReactiveFormsModule } from '@angular/forms';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';


@NgModule({
  declarations: [
    SearchPageComponent,
    SearchComponent
  ],
  imports: [
    CommonModule,
    SearchRoutingModule,
    ReactiveFormsModule,
    InfiniteScrollModule,

    CardCircleComponent,
    CardSquareComponent,
  ]
})
export class SearchModule { }
