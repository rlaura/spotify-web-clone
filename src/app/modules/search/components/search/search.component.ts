import {
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output,
  inject,
} from '@angular/core';
import { FormControl } from '@angular/forms';
import {
  Observable,
  Subject,
  Subscription,
  Unsubscribable,
  debounceTime,
  distinct,
  distinctUntilChanged,
  filter,
  map,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs';
import { SearchService } from '../../services/search.service';
import { ActivatedRoute, Router } from '@angular/router';

interface itemButton {
  name: string;
  search: string;
  param?: '';
  route?: string;
}
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit, OnDestroy {
  @Output('search') searchEmitter = new EventEmitter<String>();

  buttonGroup: itemButton[] = [
    // {
    //   name: 'Todo',
    //   search: 'album,artist,playlist,track,show,episode,audiobook',
    //   route: '',
    // },
    { name: 'todo', search: 'album,artist,playlist,track,show,episode,audiobook', route: 'all' },
    {
      name: 'Pódcasts y programas',
      search: 'audiobook',
      route: 'podcastAndEpisodes',
    },
    { name: 'Canciones', search: 'track', route: 'tracks' },
    { name: 'Álbumes', search: 'album', route: 'albums' },
    { name: 'Artistas', search: 'artist', route: 'artists' },
    { name: 'Listas', search: 'playlist', route: 'playlists' },
    { name: 'Episodios', search: 'episode', route: 'users' },
    // {
    //   name: 'Géneros y estados de ánimo',
    //   search: 'episode',
    //   route: 'genres',
    // },
  ];

  search: FormControl = new FormControl('');
  isEmptySearch: boolean = false;
  //pathRoute: string = 'album,artist,playlist,track,show,episode,audiobook';
  data = '';
  textSearch: any = {
    paramSearch: '',
    type: 'album,artist,playlist,track,show,episode,audiobook',
  };

  private searchService = inject(SearchService);
  private route = inject(Router);
  private activateRoute = inject(ActivatedRoute);
  private data$!: Observable<string>;
  private destroy$: Subject<boolean> = new Subject();
  listObservers$!: Subscription;

  ngOnInit(): void {
    
    this.listObservers$ = this.searchService._paramSearch$
      .pipe(takeUntil(this.destroy$))
      .subscribe((resp: any) => {
        console.log('searchParam: ', resp);
        this.search.reset(resp);
      });
    // this.listObservers$.push(observer1);
    this.escucharSearch();
  }

  goRoute(path: string) {
    this.textSearch.type = path;
    // this.searchEmitter.emit(this.textSearch);
  }

  escucharSearch() {
    this.search.valueChanges
      .pipe(
        map((search) => search.toLowerCase().trim()),
        tap((search) =>
          search.length >= 1
            ? (this.isEmptySearch = true)
            : (this.isEmptySearch = false)
        ),
        // filter((search: string) => search.length >= 3),
        debounceTime(500),
        distinctUntilChanged()
        // tap((search) => {
        //   console.log(search.length)
        //   if (search.length <= 1){
        //     this.route.navigate(['/home/search']);
        //   }
        // })
        //switchMap((dataSearch) => this.searchEmitter.emit(dataSearch))
        // tap((resp) => console.log(resp)),
        // switchMap((_) => this.searchService.getParamSearch)
      )
      .subscribe((dataSearch) => {
        // console.log(this.isEmptySearch);
        // console.log(dataSearch);
        // console.log('hola');
        console.log(dataSearch.length);
        // this.route.navigate(['/home/search']);
        if (dataSearch.length >= 1) {
          this.route.navigate(['/home/search', dataSearch,'all']);
          this.textSearch.paramSearch = dataSearch;
          this.data = dataSearch;
          // this.searchEmitter.emit(this.textSearch);
          this.searchService._paramSearch$.next(dataSearch);
        }
      });
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
    this.listObservers$.unsubscribe();
    console.log('search component destroyed');
  }
}

// buttonGroup: itemButton[] = [
//   {
//     name: 'Todo',
//     search: 'album,artist,playlist,track,show,episode,audiobook',
//     route: '/search/todo',
//   },
//   {
//     name: 'Pódcasts y programas',
//     search: 'audiobook',
//     route: '/search/palabra/audiobook',
//   },
//   { name: 'Canciones', search: 'track', route: '/search/palabra/track' },
//   { name: 'Álbumes', search: 'album', route: '/search/palabra/album' },
//   { name: 'Artistas', search: 'artist', route: '/search/palabra/artist' },
//   { name: 'Listas', search: 'playlist', route: '/search/palabra/playlist' },
//   { name: 'Perfiles', search: 'show', route: '/search/palabra/show' },
//   {
//     name: 'Géneros y estados de ánimo',
//     search: 'episode',
//     route: 'search/palabra/episode',
//   },
// ];
