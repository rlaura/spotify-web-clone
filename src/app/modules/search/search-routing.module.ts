import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SearchPageComponent } from './pages/search-page/search-page.component';

const routes: Routes = [
  {
    path: '',
    component: SearchPageComponent,
    children: [
      {
        path: ':searchParam/all',
        loadComponent: () =>
          import('./pages/all-search-page/all-search-page.component').then(
            (component) => component.AllSearchPageComponent
          ),
      },
      {
        path: ':searchParam/albums',
        loadComponent: () =>
          import(
            './pages/albums-search-page/albums-search-page.component'
          ).then((c) => c.AlbumsSearchPageComponent),
      },
      {
        path: ':searchParam/artists',
        loadComponent: () =>
          import(
            './pages/artists-search-page/artists-search-page.component'
          ).then((c) => c.ArtistsSearchPageComponent),
      },
      {
        path: ':searchParam/genres',
        loadComponent: () =>
          import(
            './pages/genres-search-page/genres-search-page.component'
          ).then((c) => c.GenresSearchPageComponent),
      },
      {
        path: ':searchParam/playlists',
        loadComponent: () =>
          import(
            './pages/playlist-search-page/playlist-search-page.component'
          ).then((c) => c.PlaylistSearchPageComponent),
      },
      {
        path: ':searchParam/podcastAndEpisodes',
        loadComponent: () =>
          import(
            './pages/podcast-search-page/podcast-search-page.component'
          ).then((c) => c.PodcastSearchPageComponent),
      },
      {
        path: ':searchParam/tracks',
        loadComponent: () =>
          import(
            './pages/tracks-search-page/tracks-search-page.component'
          ).then((c) => c.TracksSearchPageComponent),
      },
      {
        path: ':searchParam/users',
        loadComponent: () =>
          import('./pages/users-search-page/users-search-page.component').then(
            (c) => c.UsersSearchPageComponent
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SearchRoutingModule {}

// const routes: Routes = [
//   { path: '', component: SearchPageComponent },
//   {
//     path: ':id',
//     loadComponent: () =>
//       import('./pages/all-search-page/all-search-page.component').then(
//         (component) => component.AllSearchPageComponent
//       ),
//   },
//   { path: ':id/podcastAndEpisodes', component: SearchPageComponent },
//   { path: ':id/artists', component: SearchPageComponent },
//   { path: ':id/tracks', component: SearchPageComponent },
//   {
//     path: ':id/playlists',
//     loadComponent: () =>
//       import(
//         './pages/playlist-search-page/playlist-search-page.component'
//       ).then((c) => c.PlaylistSearchPageComponent),
//   },
//   { path: ':id/albums', component: SearchPageComponent },
//   { path: ':id/users', component: SearchPageComponent },
//   { path: ':id/genres', component: SearchPageComponent },
// ];
