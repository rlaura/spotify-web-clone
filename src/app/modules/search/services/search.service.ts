import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { SpotifyConfiguration } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class SearchService {
  private URI = SpotifyConfiguration.apiSpotify;
  public _paramSearch$:Subject<string> = new Subject<string>();

  constructor(private _http: HttpClient) {}

  get getParamSearch(){
    return this._paramSearch$.asObservable();
  }

  set setParamSearch(data:any){
    this._paramSearch$.next(data);
  }

  getSearch(type: string, search: string, limit: number = 30,loadMode:boolean = false , offset?: number, market?:string) {
    let params = new HttpParams()
      .set('type', type)
      .set('q', search)
      .set('limit', limit);
    
    if(loadMode) {
      // getSearchPaginate();
    }
    //.set('offset', offset);
    // console.log(params.toString());
    // console.log(this._http.get(`${this.URI}/search`,{params}))
    return this._http.get(`${this.URI}/search`,{params});
  }

  getSearch2(type: string, search: string, limit: number = 30,loadMode:boolean = false , offset?: number, market?:string) {
    let params = new HttpParams()
      .set('type', type)
      .set('q', search)
      .set('limit', limit);
  
  if(loadMode) {
    // getSearchPaginate();
  }
    return this._http.get(`${this.URI}/search`,{params});
  }

}
