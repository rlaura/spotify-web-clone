import { Component, OnInit, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchService } from '../../services/search.service';
import { ActivatedRoute } from '@angular/router';
import { Observable, switchMap, tap } from 'rxjs';
import { CardSquareComponent } from 'src/app/shared/components/card-square/card-square.component';

@Component({
  selector: 'app-podcast-search-page',
  standalone: true,
  imports: [CommonModule, CardSquareComponent],
  templateUrl: './podcast-search-page.component.html',
  styleUrls: ['./podcast-search-page.component.scss']
})
export class PodcastSearchPageComponent implements OnInit {
  private readonly type = 'show';
  public paramObservable$!: Observable<any>;

  private searchService = inject(SearchService);
  private activateRoute = inject(ActivatedRoute);

  ngOnInit(): void {
    this.paramObservable$ = this.activateRoute.params.pipe(
      tap((param) =>
        this.searchService._paramSearch$.next(param['searchParam'])
      ),
      switchMap((param) =>
        this.searchService.getSearch(this.type, param['searchParam'])
      )
    );
  }

}
