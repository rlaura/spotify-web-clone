import { Component, OnInit, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchService } from '../../services/search.service';
import { ActivatedRoute } from '@angular/router';
import { Observable, switchMap, tap } from 'rxjs';
import { CardSquareComponent } from 'src/app/shared/components/card-square/card-square.component';

@Component({
  selector: 'app-users-search-page',
  standalone: true,
  imports: [CommonModule, CardSquareComponent],
  templateUrl: './users-search-page.component.html',
  styleUrls: ['./users-search-page.component.scss']
})
export class UsersSearchPageComponent implements OnInit {
  private readonly type = 'episode';

  private searchService = inject(SearchService);
  private activateRoute = inject(ActivatedRoute);
  paramObservable$!: Observable<any>;

  ngOnInit(): void {
    this.paramObservable$ = this.activateRoute.params.pipe(
      tap((param) => {
        this.searchService._paramSearch$.next(param['searchParam']);
        console.log(param);
      }),
      switchMap((param) =>
        this.searchService.getSearch(this.type, param['searchParam'])
      )
    );
  }
}
