import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Observable, switchMap, tap } from 'rxjs';
import { MultimediaService } from 'src/app/shared/services/multimedia.service';
import { PlaylistService } from 'src/app/modules/playlist/services/playlist.service';
import { SearchService } from '../../services/search.service';
import { ActivatedRoute } from '@angular/router';
import { DurationFormatPipe } from 'src/app/shared/pipes/duration-format.pipe';

@Component({
  selector: 'app-all-search-page',
  standalone: true,
  imports: [CommonModule, DurationFormatPipe],
  templateUrl: './all-search-page.component.html',
  styleUrls: ['./all-search-page.component.scss'],
})
export class AllSearchPageComponent {
  private readonly type = 'album,artist,playlist,track,show,episode,audiobook';

  private searchService = inject(SearchService);
  private activateRoute = inject(ActivatedRoute);
  paramObservable$!: Observable<any>;

  constructor(
    private multimediaService: MultimediaService,
    private playlistService: PlaylistService
  ) {}

  ngOnInit(): void {
    console.log('hola');
    this.paramObservable$ = this.activateRoute.params.pipe(
      tap((param) => {
        this.searchService._paramSearch$.next(param['searchParam']);
        console.log(param);
      }),
      switchMap((param) =>
        this.searchService.getSearch(this.type, param['searchParam'], 4)
      )
    );
  }

  enviarDataMusic(track: any, cont: number): void {
    this.multimediaService.trackInfo$.next(track);
    this.playlistService.numLista = cont;
  }
}
