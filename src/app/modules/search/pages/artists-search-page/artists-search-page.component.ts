import { Component, OnDestroy, OnInit, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { SearchService } from '../../services/search.service';
import { Subject, switchMap, takeUntil, tap } from 'rxjs';
import { CardCircleComponent } from 'src/app/shared/components/card-circle/card-circle.component';

@Component({
  selector: 'app-artists-search-page',
  standalone: true,
  imports: [CommonModule, CardCircleComponent],
  templateUrl: './artists-search-page.component.html',
  styleUrls: ['./artists-search-page.component.scss'],
})
export class ArtistsSearchPageComponent implements OnInit, OnDestroy {
  private readonly type = 'artist';
  private destroy$ = new Subject<boolean>();

  artists!: any;
  private searchService = inject(SearchService);
  private activateRoute = inject(ActivatedRoute);

  ngOnInit(): void {
    //TODO: FORMA2: esta forma es mucho mejor porque se desuscribe cuando se cambia de componente
    this.activateRoute.params
      .pipe(
        takeUntil(this.destroy$),
        // tap((param) => console.log(param)),
        switchMap((param) =>
          this.searchService.getSearch(this.type, param['searchParam'])
        )
      )
      .subscribe((resp: any) => {
        console.log("artist: ",resp)
        this.artists = resp?.artists?.items;
      });
  }
  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
    console.log('artist-search-page component destroyed');
  }
}
