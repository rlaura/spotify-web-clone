import { Component, OnInit, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Observable, switchMap, tap } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { SearchService } from '../../services/search.service';
import { MultimediaService } from 'src/app/shared/services/multimedia.service';
import { PlaylistService } from 'src/app/modules/playlist/services/playlist.service';
import { DurationFormatPipe } from 'src/app/shared/pipes/duration-format.pipe';
import { DateAgoPipe } from 'src/app/shared/pipes/date-ago.pipe';

@Component({
  selector: 'app-tracks-search-page',
  standalone: true,
  imports: [CommonModule, DurationFormatPipe, DateAgoPipe],
  templateUrl: './tracks-search-page.component.html',
  styleUrls: ['./tracks-search-page.component.scss'],
})
export class TracksSearchPageComponent implements OnInit {
  private readonly type = 'track';

  private searchService = inject(SearchService);
  private activateRoute = inject(ActivatedRoute);
  paramObservable$!: Observable<any>;

  constructor(
    private multimediaService: MultimediaService,
    private playlistService: PlaylistService
  ) {}

  ngOnInit(): void {
    console.log('hola');
    this.paramObservable$ = this.activateRoute.params.pipe(
      tap((param) => {
        this.searchService._paramSearch$.next(param['searchParam']);
        console.log(param);
      }),
      switchMap((param) =>
        this.searchService.getSearch(this.type, param['searchParam'])
      )
    );
  }

  enviarDataMusic(track: any, cont: number): void {
    this.multimediaService.trackInfo$.next(track);
    this.playlistService.numLista = cont;
  }
}
