import { Component, OnDestroy, OnInit, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Subject } from 'rxjs';
import { SearchService } from '../../services/search.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-genres-search-page',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './genres-search-page.component.html',
  styleUrls: ['./genres-search-page.component.scss']
})
export class GenresSearchPageComponent implements OnInit, OnDestroy{
  private readonly type = 'genres';
  private destroy$ = new Subject<boolean>();
  
  artists!: any;
  private searchService = inject(SearchService);
  private activateRoute = inject(ActivatedRoute);


  ngOnDestroy(): void {
    
  }
  ngOnInit(): void {
    
  }
}
