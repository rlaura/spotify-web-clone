import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { SearchService } from '../../services/search.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.scss'],
})
export class SearchPageComponent implements OnInit, OnDestroy {
  a:any

  
  sum = 10;
  throttle = 10;
  scrollDistance = 1;
  scrollUpDistance = 2;
  direction = "";
  
  constructor(
    private searchService:SearchService,
    private router: Router,
    private activateRoute: ActivatedRoute
  ) {}
  

  ngOnInit(): void {

    // console.log(this.router.url)
    // console.log({ relativeTo: this.activateRoute })
  }

  handleSearch(searchData: any) {
    
    const { paramSearch, type } = searchData;

    this.searchService.getSearch(type,paramSearch).subscribe((resp) => {
      console.log(resp);
    });
    
    // switch (type) {
    //   case 'album':
    //     this.router.navigate([`${paramSearch}`,`albums`], { relativeTo: this.activateRoute });
    //     break;
    //   case 'artist':
    //     this.router.navigate([`${paramSearch}`,`artists`], { relativeTo: this.activateRoute });
    //     break;
    //   case 'genres':
    //     this.router.navigate([`${paramSearch}`,`genres`], { relativeTo: this.activateRoute });
    //     break;
    //   case 'playlist':
    //     this.router.navigate([`${paramSearch}`,`playlists`], { relativeTo: this.activateRoute });
    //     break;
    //   case 'audiobook':
    //     this.router.navigate([`${paramSearch}`,`podcastAndEpisodes`], { relativeTo: this.activateRoute });
    //     break;
    //   case 'track':
    //     this.router.navigate([`${paramSearch}`,`tracks`], { relativeTo: this.activateRoute });
    //     break;
    //   case 'episode':
    //     this.router.navigate([`${paramSearch}`,`users`], { relativeTo: this.activateRoute });
    //     break;
    //   // default:
    //   //   this.router.navigate([`${paramSearch}`], { relativeTo: this.activateRoute });
    //   //   break;
    // }

  }

  ngOnDestroy(): void {
    // this.searchService._paramSearch$.next('');
    console.log("search-page coomponents destroy")
  }

}

// infinite scroll
/**
 * https://github.com/mwiginton/angular-infinite-scroll-example
 * https://github.com/drawcall/angular-infinite-list
 */
