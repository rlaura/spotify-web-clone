import { Component, OnDestroy, OnInit, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardSquareComponent } from 'src/app/shared/components/card-square/card-square.component';
import { SearchService } from '../../services/search.service';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subject, switchMap, takeUntil, tap } from 'rxjs';

@Component({
  selector: 'app-playlist-search-page',
  standalone: true,
  imports: [CommonModule, CardSquareComponent],
  templateUrl: './playlist-search-page.component.html',
  styleUrls: ['./playlist-search-page.component.scss'],
})
export class PlaylistSearchPageComponent implements OnInit, OnDestroy {
  private readonly type = 'playlist';
  private destroy$ = new Subject<boolean>();

  playlists!: any;
  paramSearch: string = '';
  paramObservable$!: Observable<any>;
  //TODO: realizar inyeccion de dependencias sin el constructor
  private searchService = inject(SearchService);
  private activateRoute = inject(ActivatedRoute);

  ngOnInit(): void {
    this.paramObservable$ = this.activateRoute.params.pipe(
      tap((param) =>
        this.searchService._paramSearch$.next(param['searchParam'])
      ),
      switchMap((param) =>
        this.searchService.getSearch(this.type, param['searchParam'])
      )
    );

    //TODO: FORMA2: esta forma es mucho mejor porque se desuscribe cuando se cambia de componente
    // this.activateRoute.params
    //   .pipe(
    //     takeUntil(this.destroy$),
    //     tap(param=>console.log(param)),
    //     switchMap((param) =>
    //       this.searchService.getSearch(this.type, param['searchParam'])
    //     )
    //   )
    //   .subscribe((resp: any) => (this.playlists = resp?.playlists.items));

    // TODO: FORMA1 que funciona pero no se ve muy bien
    // this.activateRoute.params.subscribe((params) => {
    //   console.log("hola");
    //   console.log(params);
    //   // console.log(params['searchParam']);
    //   this.search(params['searchParam']);
    // });
  }

  search(query: string) {
    this.searchService.getSearch(this.type, query).subscribe((resp: any) => {
      // this.searchService.getSearch(type, dataSearch).subscribe((resp) => {
      // console.log(resp);
      // console.log(resp?.playlists.items);
      // console.log(resp?.playlists.items[0].images);
      this.playlists = resp?.playlists.items;
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
    console.log('playlist-search-page component destroyed');
  }
}
