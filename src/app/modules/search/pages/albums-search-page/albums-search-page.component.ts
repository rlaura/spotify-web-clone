import { Component, OnInit, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchService } from '../../services/search.service';
import { ActivatedRoute } from '@angular/router';
import { Observable, switchMap, tap } from 'rxjs';
import { CardSquareComponent } from 'src/app/shared/components/card-square/card-square.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

@Component({
  selector: 'app-albums-search-page',
  standalone: true,
  imports: [CommonModule, CardSquareComponent, InfiniteScrollModule],
  templateUrl: './albums-search-page.component.html',
  styleUrls: ['./albums-search-page.component.scss'],
})
export class AlbumsSearchPageComponent implements OnInit {
  private readonly type = 'album';

  sum = 100;
  throttle = 300;
  scrollDistance = 1;
  scrollUpDistance = 2;
  direction = "";

  private searchService = inject(SearchService);
  private activateRoute = inject(ActivatedRoute);
  paramObservable$!: Observable<any>;

  ngOnInit(): void {
    this.paramObservable$ = this.activateRoute.params.pipe(
      tap((param) => {
        this.searchService._paramSearch$.next(param['searchParam']);
        console.log(param);
      }),
      switchMap((param) => 
        this.searchService.getSearch(this.type, param['searchParam'])
      )
    );
  }

  onScroll(a:any) {
    
  }
}
