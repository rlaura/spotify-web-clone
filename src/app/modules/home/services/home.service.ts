import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { StorageService } from 'src/app/core/services/storage.service';
import { map, mergeMap, tap, catchError } from 'rxjs/operators';

import { SpotifyConfiguration } from 'src/environments/environment';
import { Item, PlayListSide } from '../interfaces/playlist.interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class HomeService {
  private URL = SpotifyConfiguration.apiSpotify;

  constructor(
    private _http: HttpClient,
    private storageService: StorageService
  ) {}

  /**
   * 
   * @returns retorna las lista de reproduccion que el usuario creo
   */
  getMePlaylists() {
    // TODO: agregamos los headers en el interceptor
    // const headers = new HttpHeaders()
    //   .set('Content-Type', 'application/json')
    //   .set('Authorization', `Bearer ${this.storageService.getUser()}`);
    // console.log(headers);

    return this._http.get<PlayListSide>(`${this.URL}/me/playlists`).pipe(
      map((res: PlayListSide) => {
        // console.log(res);
        return res.items;
      }),
    );
  }
}
