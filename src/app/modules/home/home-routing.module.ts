import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { SessionGuard } from 'src/app/core/guards/session.guard';

const routes: Routes = [
  {
    path: '',
    component: HomePageComponent,
    // pathMatch: 'full',
    children: [
      //TODO: si esta ruta se coloca fuera de children: [], no cargara el sidebar y en el html solo renderiza
      // lo que haya en el PlaylistModule OJO
      {
        path: 'playlist/:id',
        loadChildren: () => import('../playlist/playlist.module').then((m) => m.PlaylistModule),
        // canActivate: [SessionGuard],
        // pathMatch: 'full',
      },
      {
        path: 'search',
        loadChildren: () => import('../search/search.module').then((m) => m.SearchModule),
        // loadChildren: 'app/modules/search.module#SearchModule',
      },

    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeRoutingModule {}
