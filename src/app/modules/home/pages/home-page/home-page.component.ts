import { Component, OnInit } from '@angular/core';
import { HomeService } from '../../services/home.service';
import { Item } from '../../interfaces/playlist.interface';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
})
export class HomePageComponent implements OnInit {
  //TODO: cualquiera de las dos formas funciona
  // items: Item[] | undefined = [];
  items: Array<Item> | undefined = [];
  // linkTrack: string = 'sin datos';

  constructor(
    private homeService: HomeService,
    private router: Router,
    private activateRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.getPlaylist();
  }

  getPlaylist() {
    this.homeService.getMePlaylists().subscribe((data) => {
      this.items = data;
      // console.log('data: ',this.items)
    });
  }
  //TODO:este es una funcion llamada en el emiter de una clase Hija
  itemLinkTracksMusic(link: string) {
    // this.linkTrack = link;

    // console.log('link: ', this.linkTrack);
    //TODO: relativeTo Especifica un URI raíz para usar para la navegación relativa.
    this.router.navigate(['playlist',link], { relativeTo: this.activateRoute });
    // this.router.navigate(['/home', 'playlist']);


    //TODO: esto si funciona y se entiende
    //https://www.youtube.com/watch?v=CqiAhydKKxg
    // this.router.navigate(['playlist'], { relativeTo: this.activateRoute });
    // console.log(this.activateRoute)
    // this.router.navigate(['/home', 'playlist']);
    // this.router.navigate(['/home/playlist']);
    // this.router.navigate(['/home'+'/playlist']);

    //TODO: esto no funciona, ¿por que no funciona?
    // this.router.navigate(['/home', '/playlist']);
    // this.router.navigate(['/home','/','playlist']);
    // this.router.navigate(['/home','/','playlist']);
    // this.router.navigate(['/','home','/','playlist']);
    // console.log(this.router);
  }
}
