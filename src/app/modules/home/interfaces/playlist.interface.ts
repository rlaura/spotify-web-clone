export interface PlayListSide {
  href?:     string;
  items?:    Item[];
  limit?:    number;
  next?:     null;
  offset?:   number;
  previous?: null;
  total?:    number;
}

export interface Item {
  collaborative?: boolean;
  description?:   string;
  externalUrls?:  ExternalUrls;
  href?:          string;
  id?:            string;
  images?:        Image[];
  name?:          string;
  owner?:         Owner;
  primaryColor?:  null;
  public?:        boolean;
  snapshotID?:    string;
  tracks?:        Tracks;
  type?:          string;
  uri?:           string;
}

export interface ExternalUrls {
  spotify?: string;
}

export interface Image {
  height?: number;
  url?:    string;
  width?:  number;
}

export interface Owner {
  displayName?:  string;
  externalUrls?: ExternalUrls;
  href?:         string;
  id?:           string;
  type?:         string;
  uri?:          string;
}

export interface Tracks {
  href?:  string;
  total?: number;
}
