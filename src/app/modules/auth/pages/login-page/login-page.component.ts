import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StorageService } from 'src/app/core/services/storage.service';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
})
export class LoginPageComponent implements OnInit {
  constructor(
    private authService: AuthService,
    private storageService: StorageService,
    private router:Router
  ) {}

  ngOnInit(): void {
    this.verificarTokenUrlCallback();
  }

  abrirPaginaLogin() {
    window.location.href = this.authService.obtenerUrlLogin();
  }

  verificarTokenUrlCallback() {
    const token = this.authService.obtenerTokenUrlCallback();
    if (token) {
      this.storageService.saveUser(token);
      this.router.navigate(['/', 'home'])
    }
  }
}
