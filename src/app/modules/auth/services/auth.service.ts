import { Injectable } from '@angular/core';
import { SpotifyConfiguration } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor() {}

  obtenerUrlLogin(): string {
    const authEndpoint = `${SpotifyConfiguration.authEndpoint}?`;
    const clientId = `client_id=${SpotifyConfiguration.clientId}&`;
    const redirectUrl = `redirect_uri=${SpotifyConfiguration.redirectUrl}&`;
    const scopes = `scope=${SpotifyConfiguration.scopes.join('%20')}&`;
    const responseType = `response_type=token&show_dialog=true`;
    return authEndpoint + clientId + redirectUrl + scopes + responseType;
  }

  obtenerTokenUrlCallback() {
    // console.log(window.location.hash);
    if (!window.location.hash) {
      //error no existe # en la uri
      return '';
    }
    const params = window.location.hash.substring(1).split('&');
    const tokenparam = params[0].split('=')[1];
    // console.log(params)
    // console.log(tokenparam)
    return tokenparam;
  }



}
