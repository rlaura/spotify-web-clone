import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject, switchMap, takeUntil } from 'rxjs';
import { SpotifyConfiguration } from 'src/environments/environment';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class PlaylistService {
  private URI = SpotifyConfiguration.apiSpotify;
  private _dataPlayList: BehaviorSubject<any> = new BehaviorSubject([]);
  public numLista: number = 0;

  get dataPlaylistObservable() {
    return this._dataPlayList.asObservable();
  }

  set dataPlaylistObservable(data: any) {
    this._dataPlayList.next(data);
  }

  // getDataPlaylistObservable() {
  //   return this.dataPlayList.asObservable();
  // }

  // setDataPlaylistObservable(data: any) {
  //   this.dataPlayList.next(data);
  // }

  constructor(private _http: HttpClient) {}
  /**
   *
   * @param id es el id de la playlist
   * @returns un objeto que dentro lleva un atributo con un arreglo de la lista de reproduccion
   */
  getPlaylist(id: string) {
    return this._http.get(`${this.URI}/playlists/${id}`);
  }
}
