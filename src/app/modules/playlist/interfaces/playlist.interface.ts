export interface Playlist {
  collaborative?: boolean;
  description?:   string;
  externalUrls?:  ExternalUrls;
  followers?:     Followers;
  href?:          string;
  id?:            string;
  images?:        Image[];
  name?:          string;
  owner?:         Owner;
  primaryColor?:  null;
  public?:        boolean;
  snapshotID?:    string;
  tracks?:        Tracks;
  type?:          string;
  uri?:           string;
}

export interface ExternalUrls {
  spotify?: string;
}

export interface Followers {
  href?:  null;
  total?: number;
}

export interface Image {
  height?: number | null;
  url?:    string;
  width?:  number | null;
}

export interface Owner {
  displayName?:  string;
  externalUrls?: ExternalUrls;
  href?:         string;
  id?:           string;
  type?:         Type;
  uri?:          string;
  name?:         string;
}

export enum Type {
  Artist = "artist",
  User = "user",
}

export interface Tracks {
  href?:     string;
  items?:    Item[];
  limit?:    number;
  next?:     null;
  offset?:   number;
  previous?: null;
  total?:    number;
}

export interface Item {
  addedAt?:        Date;
  addedBy?:        Owner;
  isLocal?:        boolean;
  primaryColor?:   null;
  track?:          Track;
  videoThumbnail?: VideoThumbnail;
}

export interface Track {
  album?:            Album;
  artists?:          Owner[];
  availableMarkets?: string[];
  discNumber?:       number;
  durationMS?:       number;
  episode?:          boolean;
  explicit?:         boolean;
  externalIDS?:      ExternalIDS;
  externalUrls?:     ExternalUrls;
  href?:             string;
  id?:               string;
  isLocal?:          boolean;
  name?:             string;
  popularity?:       number;
  previewURL?:       null | string;
  track?:            boolean;
  trackNumber?:      number;
  type?:             string;
  uri?:              string;
}

export interface Album {
  albumGroup?:           string;
  albumType?:            string;
  artists?:              Owner[];
  availableMarkets?:     string[];
  externalUrls?:         ExternalUrls;
  href?:                 string;
  id?:                   string;
  images?:               Image[];
  isPlayable?:           boolean;
  name?:                 string;
  releaseDate?:          Date;
  releaseDatePrecision?: string;
  totalTracks?:          number;
  type?:                 string;
  uri?:                  string;
}

export interface ExternalIDS {
  isrc?: string;
}

export interface VideoThumbnail {
  url?: null;
}
