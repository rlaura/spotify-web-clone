import { Component, OnDestroy, OnInit, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PlaylistService } from '../../services/playlist.service';
import { Subject, switchMap, takeUntil, tap } from 'rxjs';
import { Playlist } from '../../interfaces/playlist.interface';

@Component({
  selector: 'app-playlist',
  templateUrl: './playlist.component.html',
  styleUrls: ['./playlist.component.scss'],
})
export class PlaylistComponent implements OnInit, OnDestroy, AfterViewInit {
  private destroy$ = new Subject<boolean>();

  idPlaylist: string = '';

  // dataPlaylist!: any[];
  // dataPlaylist!:Playlist | undefined;
  dataPlaylist: Playlist = {};

  constructor(
    private activateRoute: ActivatedRoute,
    private playlistService: PlaylistService
  ) {}
  ngAfterViewInit(): void {}

  ngOnInit(): void {
    this.readParamPlayList();

    //TODO: esto funciona pero es  mejor lo de arriba
    // this.activateRoute.params.subscribe((param:any) => {
    //   console.log(param.id);
    // });

    // const param = this.activateRoute.snapshot.params['id'];
    // const param = this.activateRoute.snapshot.params['id'];
    // console.log(param)
  }

  readParamPlayList() {
    //TODO: esto funciona pero estamos usando una especie de antipatron al poner un obserbable dentro de otro
    // para eso hay que usar RxJS
    // this.activateRoute.params.subscribe(({ id }) => {
    //   console.log(id);
    //   this.idPlaylist = id;
    //   this.playlistService.getPlaylist(this.idPlaylist).subscribe((resp) => {
    //     console.log(resp);
    //   });
    // });

    //TODO: esta es una forma correcta usando RxJS
    this.activateRoute.params
      .pipe(
        //tap(({ id }) => console.log(id)),
        //TODO: para completar la subscripcion al cambiar de componente
        switchMap((params: any) => this.playlistService.getPlaylist(params.id)),
        takeUntil(this.destroy$)
      )
      .subscribe((resp: any) => {
        // console.log( resp );
        // console.log(resp.tracks?.items);
        // console.log(resp.tracks?.items![0].track?.album?.images![2].url );

        //TODO: cualquiera de las dos formas funciona
        // this.playlistService.dataPlaylistObservable(resp.tracks?.items);
        this.playlistService.dataPlaylistObservable = resp.tracks?.items;

        this.dataPlaylist = resp;
      });
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
    console.log('playlist component destroyed');
  }
}
