import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlaylistRoutingModule } from './playlist-routing.module';
import { PlaylistComponent } from './pages/playlist/playlist.component';
import { PlayListHeaderComponent } from 'src/app/shared/components/play-list-header/play-list-header.component';
import { PlayListBodyComponent } from 'src/app/shared/components/play-list-body/play-list-body.component';


@NgModule({
  declarations: [
    PlaylistComponent
  ],
  imports: [
    CommonModule,
    PlaylistRoutingModule,


    PlayListHeaderComponent,
    PlayListBodyComponent,
  ]
})
export class PlaylistModule { }
