import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SessionGuard } from './core/guards/session.guard';

const routes: Routes = [
  // TODO: colocamos este redirecTo por ahora para redireccionar al login despues se cambiara
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () =>
      import('./modules/auth/auth.module').then((m) => m.AuthModule),
  },
  {
    path: 'home',
    loadChildren: () =>
      import('./modules/home/home.module').then((m) => m.HomeModule),
      canActivate: [SessionGuard],
  },
  // {
  //   path: 'playlist',
  //   loadChildren: () => import('./modules/playlist/playlist.module').then((m) => m.PlaylistModule),
  //   canActivate: [SessionGuard],
  // },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
