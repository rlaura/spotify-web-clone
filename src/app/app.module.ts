import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { sessionInterceptorProviders } from './core/interceptors/inject-session.interceptor';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,

    InfiniteScrollModule,
  ],
  providers: [sessionInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
/**
 * https://www.youtube.com/watch?v=CiJy8psehQs
 * https://www.youtube.com/watch?v=pufcNkJPvxw
 */