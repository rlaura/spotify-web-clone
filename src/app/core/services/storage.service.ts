import { Injectable } from '@angular/core';
//https://github.com/bezkoder/angular-15-jwt-auth
const USER_KEY = 'auth-token';

@Injectable({
  providedIn: 'root',
})
export class StorageService {
  
  constructor() {}

  clean(): void {
    window.sessionStorage.clear();
  }

  public saveUser(user: any): void {
    window.sessionStorage.removeItem(USER_KEY);
    window.sessionStorage.setItem(USER_KEY, JSON.stringify(user));
    // console.log("como lo guardamos")
    // console.log(JSON.stringify(user))
    //TODO: usamos JSON.stringify() porque no solo guardaremos un string sino puede ser toda una cadema
  }

  public getUser(): any {
    const user = window.sessionStorage.getItem(USER_KEY);
    if (user) {
      return JSON.parse(user);
    }

    return {};
  }

  public isLoggedIn(): boolean {
    const user = window.sessionStorage.getItem(USER_KEY);
    if (user) {
      return true;
    }

    return false;
  }
}
