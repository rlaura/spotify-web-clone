import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanLoad, Route, Router, RouterStateSnapshot, UrlSegment, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { StorageService } from '../services/storage.service';

@Injectable({
  providedIn: 'root'
})
export class SessionGuard implements CanActivate, CanLoad {

  constructor(private storageService:StorageService, private router:Router){

  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.checktokenSession();
  }
  
  canLoad(
    route: Route,
    segments: UrlSegment[]): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.checktokenSession();
  }

  checktokenSession():boolean{
    try {
      if (this.storageService.isLoggedIn()) {
        return true;
      }else{
        this.router.navigate(['/', 'login'])
        return false;
      }
    } catch (error) {
      console.log('Algo sucedio ?? 🔴', error);
      return false
    }
  }

}
