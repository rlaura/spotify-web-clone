import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HTTP_INTERCEPTORS,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { StorageService } from '../services/storage.service';

@Injectable()
export class InjectSessionInterceptor implements HttpInterceptor {
  constructor(private storageService: StorageService) {}
  //https://dev.to/ricardochl/como-usar-httpinterceptors-en-angular-2o84
  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    try {
      const token = this.storageService.getUser();
      let newRequest = request;
      newRequest = request.clone({
        setHeaders: {
          'Content-Type': 'application/json',
          authorization: `Bearer ${token}`,
        },
      });

      return next.handle(newRequest);
    } catch (error) {
      console.log('🔴🔴🔴 error', error);
      return next.handle(request);
    }
  }
}

export const sessionInterceptorProviders = [
  {
    provide: HTTP_INTERCEPTORS,
    useClass: InjectSessionInterceptor,
    multi: true,
  },
];
