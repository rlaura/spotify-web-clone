import { Injectable } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { BehaviorSubject, Observable, Observer, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MultimediaService {

  // callback: EventEmitter<any> = new EventEmitter<any>()

  public trackInfo$: BehaviorSubject<any> = new BehaviorSubject(undefined)
  public audio!: HTMLAudioElement //TODO <audio>
  public timeElapsed$: BehaviorSubject<string> = new BehaviorSubject('00:00')
  public timeRemaining$: BehaviorSubject<string> = new BehaviorSubject('-00:00')
  public playerStatus$: BehaviorSubject<string> = new BehaviorSubject('paused')
  public playerPercentage$: BehaviorSubject<number> = new BehaviorSubject(0)

  constructor() {

    this.audio = new Audio()

    this.trackInfo$.subscribe(responseOK => {
      if (responseOK) {
        this.setAudio(responseOK)
      }
    })

    this.listenAllEvents()

  }

  private listenAllEvents(): void {
    
    //https://developer.mozilla.org/en-US/docs/Web/API/HTMLMediaElement
    //https://developer.mozilla.org/en-US/docs/Web/API/EventTarget/addEventListener
    /**
     * El evento timeupdate es llamado cuando el tiempo indicado por el atributo currentTime es actualizado.
     */
    this.audio.addEventListener('timeupdate', this.calculateTime, false)
    this.audio.addEventListener('playing', this.setPlayerStatus, false)
    this.audio.addEventListener('play', this.setPlayerStatus, false)
    this.audio.addEventListener('pause', this.setPlayerStatus, false)
    this.audio.addEventListener('ended', this.setPlayerStatus, false)

  }

  private setPlayerStatus = (state: any) => {
    // console.log(state)
    // console.log(state.type)
    switch (state.type) { //TODO: --> playing
      case 'play':
        this.playerStatus$.next('play')
        break
      case 'playing':
        this.playerStatus$.next('playing')
        break
      case 'ended':
        this.playerStatus$.next('ended')
        break
      default:
        this.playerStatus$.next('paused')
        break;
    }

  }
  /**
   * metodo que se encarga de responder al evento timeupdate mediante una funcion de flecha
   */
  private calculateTime = () => {
    const { duration, currentTime } = this.audio
    // console.log("currentTime: ", currentTime)
    this.setTimeElapsed(currentTime)
    this.setRemaining(currentTime, duration)
    this.setPercentage(currentTime, duration)
  }
  /**
   * Establece en porcentaje el tiempo actual en el que se encuentra el audio
   * @param currentTime la posición de reproducción actual, en segundos.
   * @param duration  la duración en segundos del recurso multimedia
   */
  private setPercentage(currentTime: number, duration: number): void {
    //TODO duration ---> 100%
    //TODO currentTime ---> (x)
    //TODO (currentTime * 100) / duration
    let percentage = (currentTime * 100) / duration;
    this.playerPercentage$.next(percentage)
  }

  /**
   * Establece el tiempo actual en el que se encuentra la pista de audio en formato mm:ss
   * @param currentTime la posición de reproducción actual, en segundos.
   */
  private setTimeElapsed(currentTime: number): void {
    let seconds = Math.floor(currentTime % 60) //TODO 1,2,3
    let minutes = Math.floor((currentTime / 60) % 60)
    //TODO  00:00 ---> 01:05 --> 10:15
    const displaySeconds = (seconds < 10) ? `0${seconds}` : seconds;
    const displayMinutes = (minutes < 10) ? `0${minutes}` : minutes;
    const displayFormat = `${displayMinutes}:${displaySeconds}`
    this.timeElapsed$.next(displayFormat)
  }

  /**
   * establece el tiempo en retroceso que le queda a la pista de audio en formato mm:ss
   * @param currentTime la posición de reproducción actual, en segundos.
   * @param duration  la duración en segundos del recurso multimedia
   */
  private setRemaining(currentTime: number, duration: number): void {
    let timeLeft = duration - currentTime;
    let seconds = Math.floor(timeLeft % 60)
    let minutes = Math.floor((timeLeft / 60) % 60)
    const displaySeconds = (seconds < 10) ? `0${seconds}` : seconds;
    const displayMinutes = (minutes < 10) ? `0${minutes}` : minutes;
    const displayFormat = `-${displayMinutes}:${displaySeconds}`
    this.timeRemaining$.next(displayFormat)
  }


  //TODO: Funciones publicas
  /**
   * metodo que establece la url del audio a reproducir
   * @param track es el objeto que tiene la pista de audio
   */
  public setAudio(track: any): void {
    console.log('data music: ', track);
    this.audio.src = track.preview_url
    this.audio.play()
  }
  /**
   * metodo que pausa y reanuda el audio
   */
  public togglePlayer(): void {
    (this.audio.paused) ? this.audio.play() : this.audio.pause()
  }
  /**
   * metodo que establece el nuevo tiempo para reproducir el audio
   * @param percentage es el punto donde se hizo clic en porcentaje
   */
  public seekAudio(percentage: number): void {
    const { duration } = this.audio
    const percentageToSecond = (percentage * duration) / 100
    this.audio.currentTime = percentageToSecond
  }

}
