import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-card-circle',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './card-circle.component.html',
  styleUrls: ['./card-circle.component.scss']
})
export class CardCircleComponent {
  @Input() data!:any;
}
