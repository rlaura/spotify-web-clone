import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DateAgoPipe } from '../../pipes/date-ago.pipe';
import { DurationFormatPipe } from '../../pipes/duration-format.pipe';
import { MultimediaService } from '../../services/multimedia.service';
import { PlaylistService } from 'src/app/modules/playlist/services/playlist.service';

@Component({
  selector: 'app-play-list-body',
  standalone: true,
  imports: [CommonModule, DateAgoPipe, DurationFormatPipe],
  templateUrl: './play-list-body.component.html',
  styleUrls: ['./play-list-body.component.scss'],
})
export class PlayListBodyComponent {
  @Input() dataPlayList!: any;

  constructor(
    private multimediaService: MultimediaService,
    private playlistService: PlaylistService
  ) {}
  /**
   *
   * @param track
   */
  enviarDataMusic(track: any, cont: number): void {
    this.multimediaService.trackInfo$.next(track);
    this.playlistService.numLista = cont;
  }
}

// @Input() dataPlayList!: any;
// @Input() dataPlayList!: any;
// @Input() dataPlayList!: any[];

// data!: any[] | undefined;

// constructor() {}
// ngOnInit(): void {
//   console.log(this.data);
// }

// ngOnChanges(changes: SimpleChanges) {
//   console.log(changes['dataPlayList'].currentValue);
//   if (changes['dataPlayList']) {
//     this.data = this.groupByCategory(changes['dataPlayList'].currentValue);
//   }
// }

// groupByCategory(data: any[]) {
//   if (!data) return;
//   const result = data;

//   return result;
// }
