import { Component, Input } from '@angular/core';
import { CommonModule, NgOptimizedImage } from '@angular/common';
import { NotImagesDirective } from '../../directive/not-images.directive';

@Component({
  selector: 'app-card-square',
  standalone: true,
  imports: [CommonModule, 
    NotImagesDirective,
    // NgOptimizedImage
  ],
  templateUrl: './card-square.component.html',
  styleUrls: ['./card-square.component.scss']
})
export class CardSquareComponent {
  @Input() dataPlayList!:any;
}
