import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Item } from 'src/app/modules/home/interfaces/playlist.interface';

@Component({
  selector: 'app-play-list-sidebar',
  templateUrl: './play-list-sidebar.component.html',
  styleUrls: ['./play-list-sidebar.component.scss'],
})
export class PlayListSidebarComponent {
  @Input() itemListSide: Item[] | undefined = [];
  @Output() onItemLink = new EventEmitter<string>();

  emitirLink(link: string) {
    this.onItemLink.emit(link);
    // console.log(link);
  }
}
