import {
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
  ElementRef,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { MultimediaService } from '../../services/multimedia.service';
import { Subscription, Observable } from 'rxjs';
import { PlaylistService } from 'src/app/modules/playlist/services/playlist.service';

@Component({
  selector: 'app-media-player',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './media-player.component.html',
  styleUrls: ['./media-player.component.scss'],
})
export class MediaPlayerComponent implements OnInit, OnDestroy {
  @ViewChild('progressBar') progressBar: ElementRef = new ElementRef('');
  listObservers$: Array<Subscription> = [];
  state: string = 'paused';
  // dataPlaylist$!: Observable<any>;
  data: any;

  constructor(
    public multimediaService: MultimediaService,
    public playlistService: PlaylistService
  ) {}

  ngOnInit(): void {
    const observer1$ = this.multimediaService.playerStatus$.subscribe(
      (status) => {
        this.state = status;
        console.log("status component",status)
        // if(status === "ended"){
        //   this.nextMusic();
        // }
      }
    );
    const observer2$ = this.playlistService.dataPlaylistObservable.subscribe(
      (resp: any) => {
        this.data = resp;
        console.log(resp);
      }
    );
    // console.log('dataPlaylist$', this.dataPlaylist$);
    this.listObservers$ = [observer1$, observer2$];
  }

  ngOnDestroy(): void {
    this.listObservers$.forEach((u) => u.unsubscribe());
    console.log('mediaplyayer component destruido');
  }

  // handlePosition($event:MouseEvent): void {
  //   console.log("event: " , $event)
  //   const elNative: HTMLElement = this.progressBar.nativeElement;
  //   console.log("elNative: " , elNative)
  //   const clientX = $event.clientX;
  //   console.log("clientX: " + clientX)
  //   const { x, width } = elNative.getBoundingClientRect();
  //   console.log("elNative.getBoundingClientRect().x: " , elNative.getBoundingClientRect().x)
  //   console.log("elNative.getBoundingClientRect().width: " , elNative.getBoundingClientRect().width)
  //   console.log("elNative.getBoundingClientRect().y: " , elNative.getBoundingClientRect().y)
  //   const clickX = clientX - x; //TODO: 1050 - x
  //   const percentageFromX = (clickX * 100) / width;
  //   console.log(`Click(x): ${percentageFromX}`);
  //   this.multimediaService.seekAudio(percentageFromX);
  // }

  handlePosition(event: MouseEvent): void {
    const elNative: HTMLElement = this.progressBar.nativeElement;
    const { clientX } = event;
    const { x, width } = elNative.getBoundingClientRect();
    const clickX = clientX - x;
    const percentageFromX = (clickX * 100) / width;
    // console.log(`Click(x): ${percentageFromX}`);
    this.multimediaService.seekAudio(percentageFromX);
  }

  nextMusic() {
    // this.multimediaService.trackInfo$.next(track);
    
  }
  previousMusic() {
    // this.multimediaService.trackInfo$.next(track);
  }
}
