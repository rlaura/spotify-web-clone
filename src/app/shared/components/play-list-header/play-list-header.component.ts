import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-play-list-header',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './play-list-header.component.html',
  styleUrls: ['./play-list-header.component.scss']
})
export class PlayListHeaderComponent {
  @Input() dataPlayList!:any;
  // en el img colocar una imagen por defecto
}
