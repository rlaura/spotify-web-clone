import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'durationFormat',
  pure: false,
  standalone: true,
})
export class DurationFormatPipe implements PipeTransform {
  /**
   *
   * @param tiempo es la duracion en microsegundos
   * @param formato es el tipo de formato que se desea presentar
   */
  transform(tiempo: number, formato: string): any {
    let dias: number;
    let horas: number;
    let minutos: number;
    let segundos: number;

    if (formato === 'hhmmss') {
      segundos = Math.floor((tiempo / 1000) % 60);
      minutos  = Math.floor(((tiempo / (1000 * 60))  % 60));
      horas    = Math.floor(tiempo / (1000 * 60 * 60));

      return this.formatear(formato, segundos, minutos,horas,(dias = 0));
    }

    if (formato === 'ddhhmmss') {
      segundos = Math.floor((tiempo / 1000) % 60);
      minutos  = Math.floor(((tiempo / (1000 * 60))  % 60));
      horas    = Math.floor(tiempo / (1000 * 60 * 60));
      dias     = Math.floor((tiempo / (1000 * 60 * 60 * 24)));

      return this.formatear(formato, segundos, minutos,horas,dias);
    }


    return tiempo;
  }

  // private formatear(formato:string, segundos:number, minutos:number,horas:number, dias:number ) {
  private formatear(formato:string, segundos:any, minutos:any,horas:any, dias:any ) {
    (dias < 10) ? dias = `0${dias}` : dias;
    (horas < 10) ? horas = `0${horas}` : horas;
    (minutos < 10) ? minutos = `0${minutos}` : minutos;
    (segundos < 10) ? segundos = `0${segundos}` : segundos;

    switch (formato) {
      case 'hhmmss':{
        return (horas <= 0 ) ? `${minutos}:${segundos}` : `${horas}:${minutos}:${segundos}`;
      }
      case 'ddhhmmss':{
        //TODO:faltan algunos casos mas
        if (dias <= 0) {
          return `${horas}h, ${minutos}mim, ${segundos}s`;
        }
        if ( horas <= 0 ){
          return `${minutos}mim, ${segundos}s`;
        }
        return `${dias}d, ${horas}h, ${minutos}mim, ${segundos}s`;
      }
    }

    return `${horas}:${minutos}:${segundos}`;
  }
}
