import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateAgo',
  pure: false,
  standalone: true,
})
export class DateAgoPipe implements PipeTransform {
  transform(value: any, ...args: unknown[]): unknown {
    if (value) {
      const segundos = Math.floor((+new Date() - +new Date(value)) / 1000);
      if (segundos < 29)
        // hace menos de 30 segundos se mostrará como 'justo ahora'
        return 'justo ahora';
      const intervalos: any = {
        año: 31536000,
        mes: 2592000,
        semana: 604800,
        dia: 86400,
        hora: 3600,
        minuto: 60,
        segundo: 1,
      };
      let contador;
      for (const i in intervalos) {
        contador = Math.floor(segundos / intervalos[i]);
        if (contador > 0)
          if (contador === 1) {
            return 'hace ' + contador + ' ' + i; // singular
          } else {
            return 'hace ' + contador + ' ' + i + 's'; // plural
          }
      }
    }
    return value;
  }
}

/**
  https://medium.com/@thunderroid/angular-date-ago-pipe-minutes-hours-days-months-years-ago-c4b5efae5fe5
  https://medium.com/@thunderroid/angular-date-ago-pipe-minutes-hours-days-months-years-ago-c4b5efae5fe5
  https://pratikpokhrel51.medium.com/creating-custom-dateago-pipe-in-angular-5cbf39e9c9a6
  https://www.angularjswiki.com/angular/angular-date-pipe-formatting-date-times-in-angular-with-examples/
  https://medium.com/javascript-everyday/managing-date-formats-in-angular-apps-d5e675b2f951

 */
