import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: 'img[appNotImages]',
  standalone: true
})
export class NotImagesDirective {
  @Input() urlImg!: string
  constructor(private elementRef: ElementRef) { 
    // console.log(`${window.location.origin}/assets/images/not-img.png`)
  }


  @HostListener('error')
  onError() {
    // const element = this.elementRef.nativeElement
    // console.log("hola")
    // element.src = this.urlImg || `${window.location.origin}/assets/images/not-img.png`

    this.elementRef.nativeElement.style.display = "none";

  }

}
